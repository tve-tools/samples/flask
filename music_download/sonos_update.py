#!/home/teggy/ve-soco/bin/python

# usage (sample): ./sonos_update.py

import soco

device = soco.discovery.any_soco()

print(device.music_library.library_updating)
device.music_library.start_library_update('WMP')
print(device.music_library.library_updating)

