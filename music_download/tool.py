import logging
import urlparse
import subprocess
import time
import select
import os
import shlex
import tempfile
import threading
import io
import re
from mutagen.easyid3 import EasyID3
from shutil import copy
import soco


def process_urls_async(app, url, lock, data):
  scriptPath = os.path.dirname(__file__)
#  scriptPath = tempfile.gettempdir()
#  cmd = 'docker run --rm -v ' + scriptPath + ':/user-data -w /user-data essai-dckr:latest /docker-run.sh'
#  cmd = 'docker run --rm -v ' + scriptPath + ':/user-data -w /user-data youtube-dl:latest /docker-run.sh'
  cmd = '/home/teggy/tools/youtube-dwn/yt-dlp-wrapper.sh ' + scriptPath
#  url = expand_url(url)

  fd, filename = tempfile.mkstemp()
  app.logger.info(filename)
  if data is not None:
    for already_processed_urls in data:
      if already_processed_urls[3] == url:
        app.logger.info(url + ' already processed; skipping ...')
        return

  args = shlex.split(cmd + ' ' + url)
  app.logger.info("args: " + " ".join(args))
  p = subprocess.Popen( args,
    stdout=fd, 
    stderr=subprocess.PIPE,
    universal_newlines=True) #, bufsize=1)
  lock.acquire()
  data.append((p, fd, filename, url, threading.currentThread()))
  lock.release()
  app.logger.info('tool-proc: ' + str(threading.currentThread()))

  p.wait()
  app.logger.info('docker command retcode: ' + str(p.returncode))
#  music_repo = '/net/maisel01/music/Youtube'
  music_repo = '/vol1/music/Youtube'
  app.logger.info('process_mp3: ' + ' - '.join([filename, url, music_repo]))
  process_mp3(app, filename, url, music_repo)
  os.close(fd)
  os.remove(filename)


def expand_url(url):
  if 'youtu.be/' not in url:
    return(url)

 # sample: https://youtu.be/8NyParAaNos
  url_id = url[url.rfind('/')+1:]
  return('https://www.youtube.com/watch?v=' + url_id)

  
def get_status(app, elems):
  if elems is None:
    return('please provide url...')

  msg = ''
  hasAliveThreads = False
  for i, elem in enumerate(elems):
#    elem[4].join(0.1)
    if not elem[4].is_alive():
      msg += elem[3] + ' - done (thread completed)<br/>\n'
    else:
      if not os.path.isfile(elem[2]):
        msg += elem[3] + ' - temp file deleted<br/>\n'
        continue

      file = io.open(elem[2], 'r', encoding='utf-8')
      data = file.read()
      file.close()
      data = data.replace('\r\n', '\n')
      data = data.replace('\n', '<br/>\n')
      msg += '<br/>\n' + elem[3] + '<br/>\n' + data
      hasAliveThreads = True

  if hasAliveThreads:
    msg += '<meta http-equiv="refresh" content="3" />\n'
  else:
    device = soco.discovery.any_soco()
    device.music_library.start_library_update()
#    sonos_update_ongoing = str(device.music_library.library_updating)
#    msg += ' - sonos_update_ongoing: ' + str(device.music_library.library_updating)
    if device.music_library.library_updating:
      msg += ' - Sonos library is being updated...'
    else:
      msg += ' - Sonos library is NOT being updated !'



  return(msg)


def filter_url(str):
  if "stackoverflow" not in str and \
    "youtube" not in str and \
    "youtu.be" not in str and \
    "wikipedia" not in str:
    return False

  url_decomp = urlparse.urlsplit(str)
  if url_decomp[0]:
    return True
  else:
    return False
  

def process_mp3(app, tempfile, url, music_repo):
  filename, a, t = get_tag_info(app, tempfile, url)
  app.logger.info(', '.join([filename, a, t]))
  tmpdir = os.path.dirname(__file__)
  set_tags(tmpdir + '/' + filename, a, t)
  app.logger.info('tmpdir: ' + tmpdir + ', ' + filename)
  move_mp3(app, tmpdir + '/' + filename, a, t, music_repo)
#  subprocess.Popen( 'chgrp partage ' + music_repo + '/' + filename, 
#    shell=True).communicate()


def get_tag_info(app, tempfile, url):
  logline = ''
  with io.open(tempfile, 'r', encoding='utf-8') as f:
    for line in f:
#      app.logger.info('log: ' + line)
      if 'ExtractAudio' in line:
        logline = line
        break

  filename = logline.rstrip().replace('[ExtractAudio] Destination: ', '')
  filename = os.path.basename(filename)
  filename = filename.encode( 'utf-8', errors='ignore').decode('utf-8')

  app.logger.info(filename)
  s = re.sub('.mp3\s*$', '', filename)
  s = "".join(c for c in s if ord(c) < 128)
  s = s.lower()
  s = s.replace('official', '')\
    .replace('video', '')\
    .replace('download', '')\
    .replace('with lyrics', '')\
    .replace('lyrics', '')\
    .replace('with lyric', '')\
    .replace('lyric', '')\
    .replace('music', '')\
    .replace('4k', '')\
    .replace('[', '')\
    .replace(']', '')\
    .replace('(', '')\
    .replace(')', '')\
    .replace('link', '')

  s = re.sub('_+', '_', s)
  s = re.sub('_', ' ', s)
  s = re.sub(' +', ' ', s)
  s = re.sub('\(\s*\)', '', s)

  app.logger.info('s: ' + s)

  arr = s.split(' - ')
  artist = arr.pop(0).strip()
  artist = artist.strip()
  artist = artist.title()
  title = " ".join(arr).strip()
  title = title.strip()
  title = title.capitalize()

  return filename, artist, title


def set_tags(filename, artist, title):
  audio = EasyID3(filename)
  audio["artist"] = artist.encode('utf-8')
  audio["title"] = title.encode('utf-8')
#  audio["artist"] = unicode(artist, 'utf-8').encode('ascii', errors='ignore').decode('ascii')
#  audio["title"] = unicode(title, 'utf-8').encode('ascii', errors='ignore').decode('ascii')
  audio.save()


def move_mp3(app, filename, artist, title, music_repo, delete_local_copy=True):
  newfilename = artist + " - " + title + ".mp3"
#  os.rename(filename, newfilename)
  try:
#    result = subprocess.check_output(shlex.split('/home/teggy/flask/music_download/tmp-mount-music-repo.sh'))
    copy(filename, music_repo + '/' + newfilename)
  except OSError:
    app.logger.info('OS warning...')

  if delete_local_copy:
    print('local copy to be deleted...')
    os.remove(filename)

