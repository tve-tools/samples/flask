
# usage: python music_download_app.py

import logging
from flask import Flask, redirect, render_template, request, session, Markup, send_from_directory
from tool import process_urls_async, get_status, filter_url
from threading import Thread, Lock
import threading
import os

app = Flask(__name__, static_url_path='/music/download/static')
app.config.from_object(__name__)

lock = Lock()
data = []

@app.route("/")
def index():
    return "Flask App!"

@app.route("/music/download/")
def music_download():
  return render_template('music-download.html')

@app.route("/music/cut/")
def music_cut():
  return render_template('music-cut.html')

@app.route("/video/convert/")
def video_convert():
  return render_template('video_convert.html')

@app.route("/music/download/process", methods=['POST'])
def music_download_process():
  urlstr = request.form.to_dict(flat=True).get('urls')
  urls = filter(filter_url, urlstr.split())

  for url in urls:
    thread = Thread(target=process_urls_async, args=(app, url, lock, data, ))
    thread.daemon = False
    thread.start()

  return redirect("/music/download/status", code=302)

@app.route("/music/download/status", methods=['GET', 'POST'])
def music_download_status():
  return generate_msg(get_status(app, data))


@app.route("/music/download/info", methods=['GET', 'POST'])
def music_download_info():
  return generate_msg(mount_smb(app))

def generate_msg(s):
  return render_template('message.html', message=Markup(s))

# https://www.favicon.cc/
@app.route("/favicon.ico")
def favicon():
  return send_from_directory(os.path.join(app.root_path, 'static'),
    'favicon.ico',mimetype='image/vnd.microsoft.icon')

if __name__ == "__main__":
#  gunicorn_logger = logging.getLogger('gunicorn.error')
#  app.logger.handlers = gunicorn_logger.handlers
#  app.logger.setLevel(gunicorn_logger.level)

  logHandler = logging.FileHandler('/home/teggy/logs/music-app.log')
  logHandler.setLevel(logging.INFO)
  app.logger.addHandler(logHandler)
  app.logger.setLevel(logging.INFO)

  app.run()
#  app.run(host = '0.0.0.0', port=5000, debug=True)
