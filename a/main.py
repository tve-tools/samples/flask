
# usage: ~/venv2-7/bin/python main.py

import logging
from flask import Flask, render_template, request, Markup, send_from_directory
import tool
from threading import Thread
import os

app = Flask(__name__, static_url_path='/static')
app.config.from_object(__name__)

#lock = Lock()

@app.route("/video/fetch/")
def video_convert():
  return render_template('video_convert.html')

@app.route("/video/fetch/process", methods=['POST'])
def video_fetch_process():

  status, pid, count, pinfo = tool.run_ps(app, myfilter = 'processmediafile')
  if status:
    return generate_msg(str(count) + ' ongoing conversion(s); pid: ' + str(pid) + ' {{ ' + pinfo + ' }}')

  in_filters = request.form.to_dict(flat=True).get('filter')

  in_onRaidPartition = False
  if request.form.get('onRaidPartition'):
    in_onRaidPartition = True

  vidFilter = None
  if not isinstance(in_filters, basestring):
    vidFilter = in_filters[0]
  else:
    vidFilter = in_filters

  if vidFilter:
    app.logger.info(vidFilter)
    thread = Thread(target = tool.launch_on_demand_script, args=(app, vidFilter, in_onRaidPartition))
    thread.start()
    status, pid, count, pinfo = tool.run_ps(app, myfilter = 'on-demand')
    msg = ''
    if status:
      msg = str(count) + ' ongoing conversion(s); pid: ' + str(pid) + ' {{ ' + pinfo + ' }}'

    return generate_msg('done: ' + vidFilter + '<br/>' + msg)

  return generate_msg('No filter provided; not doing anything...')

def generate_msg(s):
  return render_template('message.html', message=Markup(s))

# https://www.favicon.cc/
@app.route("/favicon.ico")
def favicon():
  return send_from_directory(os.path.join(app.root_path, 'static'),
    'favicon.ico',mimetype='image/vnd.microsoft.icon')

if __name__ == "__main__":
  app.run(host = '0.0.0.0', port=5001, debug=True)
