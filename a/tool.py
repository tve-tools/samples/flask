import logging
import subprocess
import os
import shlex
import threading
import ftplib


def launch_on_demand_script(app, vidFilter, onRaidPartition = True):
#  script = '$HOME/tools/mm/on-demand.sh chimp y > $HOME/logs/on-demand.cronlog'
  script = '/home/teggy/tools/mm/on-demand.sh'
#  script = '/bin/echo'
  logfilename = '/home/teggy/logs/on-demand.weblog'

  arg2 = 'y'
  if not onRaidPartition: arg2 = 'n'

  logfile = open(logfilename, 'w')
  cmd = [ script, vidFilter, arg2 ]
  app.logger.info('about to launch: ' + " ".join(cmd))
  p = subprocess.Popen( cmd,
    stdout = logfile, stderr = subprocess.STDOUT, 
#    universal_newlines=True)
    universal_newlines=True, preexec_fn=os.setpgrp ) #, bufsize=1)


def run_ps(app, myfilter):
  cmd = 'ps -fe'

  p = subprocess.Popen(
    shlex.split(cmd), stdout = subprocess.PIPE, universal_newlines = True )

  stdout = p.communicate()[0].splitlines()
  matchedProcs = filter(lambda line: myfilter in line, stdout)
#  app.logger.info("content: " + '--'.join(matchedProcs))

  if matchedProcs:
    pid = matchedProcs[0].split()[1]
    pstarttime = matchedProcs[0].split()[4]
    pinfo = matchedProcs[0].split()[7:]
    return True, pid, len(matchedProcs), pstarttime + " - " + (" ".join(pinfo))
  else:
    return False, -1, 0, ""

def ftp_list_files(app, myfilter):
	ftp = ftplib.FTP("mafreebox.freebox.fr")
	ftp.login()

#	data = []

#	ftp.dir(data.append)
	ftp.cwd('/Disque%20dur/Enregistrements')

	ftp.quit()

#	for line in data:
#    print "-", line

