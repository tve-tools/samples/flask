import logging
import urlparse
import subprocess
import time
import select
import os

def read_all_so_far(proc, retVal=''): 
  while (select.select([proc.stdout],[],[],0)[0]!=[]):   
    retVal+=proc.stdout.read(1)
  return retVal


def process_urls_async(app, urlstr, lock, arr):
  app.logger.info("urlstr: " + urlstr)
  urls = filter(filter_url, urlstr.split())
  for url in urls:
    app.logger.info("url: " + url)
    scriptPath = os.path.dirname(__file__)
    p = subprocess.Popen([ 
        'docker', 'run', '--rm', '-v', scriptPath + ':/user-data', '-w', '/user-data', 'essai-dckr:latest', '/docker-run.sh', url 
#        'docker', 'run', '--rm', '-v', scriptPath + ':/user-data', '-w', '/user-data', 'youtube-dl:latest', '/docker-run.sh', url 
        ],
      stdout=subprocess.PIPE, 
      stderr=subprocess.PIPE,
      universal_newlines=True, bufsize=1)
    app.logger.info('01')
    lock.acquire()
    arr.append(p)
    lock.release()
#    stdout, stderr = p.communicate()

def get_status(app, proc):
  app.logger.info('start get_status')
  if proc.poll() is None:
    s = read_all_so_far(proc)
    if 'top 01' in s: app.logger.info('top 101 received')
    if 'top 02' in s: app.logger.info('top 102 received')
    if 'top 03' in s: app.logger.info('top 103 received')
    if 'top 04' in s: app.logger.info('top 104 received')
#      time.sleep(0.4)

def get_process_output(app, proc):
    app.logger.info('done')
    while True:
      app.logger.info(proc.stdout.readline())
      if line == '':
        break

    app.logger.info('stdout: ' + proc.stdout.readline())
    app.logger.info('stderr: ' + proc.stderr.readline())

def filter_url(str):
  if "stackoverflow" not in str and "youtube" not in str and "wikipedia" not in str:
    return False

  url_decomp = urlparse.urlsplit(str)
  if url_decomp[0]:
    return True
  else:
    return False
  
  
# msg
