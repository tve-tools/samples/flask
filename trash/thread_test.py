from threading import Thread, Lock  # <----

def f(l, i, n):
    l.acquire()
    i.append(n)
    l.release()
    print "in:", i

if __name__ == '__main__':
    lock = Lock()

    i = []
    for num in range(10):
        p = Thread(target=f, args=(lock, i, num))  # <----
        p.start()
        p.join()

    print "out:", i
