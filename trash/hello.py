# usage: python hello.py
# go to URL: https://dbebpprdlx5262.fr.world.socgen:5000/hello


# usage (old): FLASK_APP=hello.py flask run --host=0.0.0.0
# usage (old): $ openssl req -x509 -newkey rsa:4096 -nodes -out cert.pem -keyout key.pem -days 365
# usage (old): $ FLASK_APP=hello.py flask run --host=0.0.0.0 --cert cert.pem --key key.pem

from flask import Flask
import subprocess

app = Flask(__name__)

@app.route('/hello')
def hello_world():
#    process = subprocess.Popen(['echo', 'More output'],
    process = subprocess.Popen(['ls', '-ltr', '/home'],
      stdout=subprocess.PIPE, 
#      universal_newlines=True, bufsize=1, 
#      universal_newlines=True,
      stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
#    return 'Hello, World!'
    return stdout.decode('utf-8')

if __name__ == "__main__":
  app.run(host = '0.0.0.0', ssl_context=('cert.pem', 'key.pem'))
#  app.run(host='0.0.0.0', ssl_context='adhoc')
