# https://pythonspot.com/flask-web-app-with-python/
# usage: python music-download.py
from flask import Flask, flash, redirect, render_template, request, session, abort, jsonify
import logging
import urlparse
import subprocess
import time
import select

app = Flask(__name__)

@app.route("/")
def index():
    return "Flask App!"

@app.route("/hello/<string:name>/")
def hello(name):
    return render_template('test.html', name=name)

@app.route("/music/download")
def music_download():
  return render_template('music-download.html')

@app.route("/music/essai")
def music_essai():
  return render_template('myjs.html')

@app.route("/music/download/process", methods=['POST'])
def music_download_process():
  urlstr = request.form.to_dict(flat=True).get('urls')
  process(urlstr)
  return jsonify(urlstr)

def readAllSoFar(proc, retVal=''): 
  while (select.select([proc.stdout],[],[],0)[0]!=[]):   
    retVal+=proc.stdout.read(1)
  return retVal


#        'ls', '-ltr', '/home'
def process(urlstr):
  app.logger.info("urlstr: " + urlstr)
  urls = filter(filter_url, urlstr.split())
  for url in urls:
    app.logger.info("url: " + url)
    p = subprocess.Popen([ 
        'docker', 'run', '--rm', '-v', '/home/X195076/essai-flask:/user-data', '-w', '/user-data', 'essai-dckr:latest', '/docker-run.sh', url 
        ],
      stdout=subprocess.PIPE, 
      stderr=subprocess.PIPE)
    app.logger.info('01')
#    stdout, stderr = p.communicate()
    app.logger.info('02')
#    app.logger.info(stdout.decode('utf-8'))
    while p.poll() is None:
      s = readAllSoFar(p)
      if 'top 01' in s: app.logger.info('top 101 received')
      if 'top 02' in s: app.logger.info('top 102 received')
      if 'top 03' in s: app.logger.info('top 103 received')
      if 'top 04' in s: app.logger.info('top 104 received')
      time.sleep(0.4)

    app.logger.info('done')
    stdout, stderr = p.communicate()
    app.logger.info('stdout: ' + stdout.decode('utf-8'))
    app.logger.info('stderr: ' + stderr.decode('utf-8'))

def filter_url(str):
  if "stackoverflow" not in str and "youtube" not in str and "wikipedia" not in str:
    return False

  url_decomp = urlparse.urlsplit(str)
  if url_decomp[0]:
    return True
  else:
    return False
  
  

if __name__ == "__main__":
  app.run(host = '0.0.0.0', port=5000, debug=True)
