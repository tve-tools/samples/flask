import re
#from mutagen.easyid3 import EasyID3
import os
from shutil import copy
#from os.path import copy

str = 'Lady___Gaga___-____Bad_Romance - sinqd .mp3'
url = 'https://www.youtube.com/watch?v=oxHnRfhDmrk'
line = '[ffmpeg] Destination: Don_McLean_-_Vincent_Starry_Starry_Night_With_Lyrics-oxHnRfhDmrk.mp3  '


def get_tag_info(logline, url):
  s = re.sub('.ffmpeg. Destination:\s*', '', logline)
  filename = s.strip()
  s = re.sub('.mp3\s*$', '', s)
  s = re.sub('_+', '_', s)
  s = re.sub('_', ' ', s)
  arr = []
  download_id = ''
  for e in s.split('-'):
    if e.strip() in url:
      download_id = e.strip()
    else:
      arr.append(e.strip())

  artist = arr.pop(0)
  title = " ".join(arr)
  return filename, artist, title, download_id

def set_tags(filename, artist, title, download_id):
  audio = EasyID3(filename)
  audio["title"] = unicode(title, "utf-8")
  audio["artist"] = unicode(artist, "utf-8")
  audio.save()
  os.rename(filename, artist + " - " + title + ".mp3")
#  print(audio.info.length)
#  print(audio.info.title)


fn, a, t, id = get_tag_info(line, url)
print(fn)
print(a)
print(t)
print(id)

#set_tags('./Don_McLean_-_Vincent_Starry_Starry_Night_With_Lyrics-oxHnRfhDmrk.mp3', a, t, id)
#copy('Don McLean - Vincent Starry Starry Night With Lyrics.mp3', '/run/user/1000/gvfs/smb-share:server=192.168.23.22,share=music')
