import logging
#import urlparse
from urllib.parse import urlparse
import subprocess
import time
import select
import os
import shlex

def read_all_so_far(proc, retVal=''): 
  while (select.select([proc.stdout],[],[],0)[0]!=[]):   
    retVal+=proc.stdout.read(1)
  return retVal


async def process_urls_async(app, urlstr):
  app.logger.info("urlstr: " + urlstr)
  scriptPath = os.path.dirname(__file__)
  cmd = 'docker run --rm -v ' + scriptPath + ':/user-data -w /user-data essai-dckr:latest /docker-run.sh'

#        'docker', 'run', '--rm', '-v', scriptPath + ':/user-data', '-w', '/user-data', 'essai-dckr:latest', '/docker-run.sh', url 
#        'docker', 'run', '--rm', '-v', scriptPath + ':/user-data', '-w', '/user-data', 'youtube-dl:latest', '/docker-run.sh', url 

  urls = filter(filter_url, urlstr.split())
  for url in urls:
    args = shlex.split(cmd + ' ' + url)
    app.logger.info("args: " + " ".join(args))
#    p = subprocess.Popen( args,
#      stdout=subprocess.PIPE, 
#      stderr=subprocess.PIPE,
#      universal_newlines=True) #, bufsize=1)

    p = await asyncio.create_subprocess_exec(
        args,
        stdout=asyncio.subprocess.PIPE)
    app.logger.info('01')
    return(p)

async def get_status(app, proc):
  app.logger.info('start get_status')
  data = await proc.stdout.readline()
  line = data.decode('ascii').rstrip()
  return(line)

def get_process_output(app, proc):
    app.logger.info('done')
    while True:
      line = proc.stdout.readline()
      if line == '': break
      app.logger.info(line)

    app.logger.info('stdout: ' + proc.stdout.readline())
    app.logger.info('stderr: ' + proc.stderr.readline())

def filter_url(str):
  if "stackoverflow" not in str and "youtube" not in str and "wikipedia" not in str:
    return False

#  url_decomp = urlparse.urlsplit(str)
  url_decomp = urlparse(str)
  if url_decomp[0]:
    return True
  else:
    return False
  
  
# msg
