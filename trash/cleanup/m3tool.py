import logging
import urlparse
import subprocess
import time
import select
import os
import shlex
import tempfile
import threading
import io

def read_all_so_far(proc, retVal=''): 
  while (select.select([proc.stdout],[],[],0)[0]!=[]):   
    retVal+=proc.stdout.read(1)
  return retVal

def get_temp_filename():
  return tempfile.mkstemp()

def process_urls_async(app, urlstr, lock, data):
  app.logger.info("urlstr: " + urlstr)
  scriptPath = os.path.dirname(__file__)
  cmd = 'docker run --rm -v ' + scriptPath + ':/user-data -w /user-data essai-dckr:latest /docker-run.sh'

#        'docker', 'run', '--rm', '-v', scriptPath + ':/user-data', '-w', '/user-data', 'youtube-dl:latest', '/docker-run.sh', url 

  fd, filename = get_temp_filename()
  app.logger.info(filename)
  urls = filter(filter_url, urlstr.split())
  for url in urls:
    already_processed = False
    if data is not None:
      for already_processed_urls in data:
        if already_processed_urls[3] == url:
          app.logger.info(url + ' already processed; skipping ...')
          already_processed = True
          continue
    if already_processed:
      continue

    args = shlex.split(cmd + ' ' + url)
    app.logger.info("args: " + " ".join(args))
    p = subprocess.Popen( args,
      stdout=fd, 
      stderr=subprocess.PIPE,
      universal_newlines=True) #, bufsize=1)
    app.logger.info('01')
    lock.acquire()
    data.append((p, fd, filename, url, threading.currentThread()))
    lock.release()

    p.wait()
    app.logger.info('docker command retcode: ' + str(p.returncode))
    retcode = mount_smb(app, fd)
    app.logger.info('docker command retcode: ' + str(retcode))


def get_status(app, elems):
  if elems is None:
    return('please provide url...')

  msg = ''
  for i, elem in enumerate(elems):
    if elem[1] is None:
      msg += elem[3] + ' - done <br/>\n'
      continue

#    if elem[0].poll() is not None:
    if not elem[4].isAlive:
      os.close(elem[1])
      os.remove(elem[2])
      fname = elem[2]
      elems[i] = (elem[0], None, None, elem[3])
      msg += 'temp file deleted: ' + fname +'<br/>\n'
    else:
#      file = open(elem[2], 'r')
      file = io.open(elem[2], 'r', encoding='utf-8')
      data = file.read()
      file.close()
      data = data.replace('\r\n', '\n')
      data = data.replace('\n', '<br/>\n')
      msg += '<br/>\n' + elem[3] + '<br/>\n' + data

#  msg += '<script>update_essaijs</script>\n'
  msg += '<meta http-equiv="refresh" content="7" />\n'
  return(msg)

def filter_url(str):
  if "stackoverflow" not in str and "youtube" not in str and "wikipedia" not in str:
    return False

  url_decomp = urlparse.urlsplit(str)
  if url_decomp[0]:
    return True
  else:
    return False
  
def mount_smb(app, fd):
#  p = subprocess.Popen(
#    shlex.split('ls -l /run/user/1000/gvfs/smb-share\:server\=192.168.23.22\,share\=music/')
#    , stdout=fd, stderr = subprocess.STDOUT, universal_newlines=True ) 
#
#  p.wait()
#  app.logger.info('ls cmd ' + str(p.returncode))
  if not os.path.exists('/run/user/1000/gvfs/smb-share:server=192.168.23.22,share=music'):
#  if p.returncode != 0:
#    p = subprocess.Popen(
#      shlex.split('ls -l /tmp')
#      , stdout=fd, stderr = subprocess.STDOUT, universal_newlines=True ) 

#    inp = open(os.path.expanduser('~/misc/essai-flask/cleanup/inp.txt'), 'r')
    inp = open(os.path.expanduser('~/.maisel01.txt'), 'r')
#    p = subprocess.Popen(
#      shlex.split('/home/X195076/misc/essai-flask/cleanup/essai.sh')
#      , stdout=fd, stderr = subprocess.STDOUT, stdin = inp , universal_newlines=True ) 
    p = subprocess.Popen(
      shlex.split('gio mount smb://192.168.23.22/music')
      , stdout=fd, stderr = subprocess.STDOUT, stdin = inp, universal_newlines=True ) 
    p.wait()
    inp.close()
    app.logger.info('gio cmd ' + str(p.returncode))

  return(p.returncode)

def mount_smb2():
  p = subprocess.Popen(shlex.split('ls -l /tmp')) 
  p.wait()
  return(str(p.returncode))
