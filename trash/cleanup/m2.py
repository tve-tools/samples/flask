from flask import Flask, redirect, render_template, request, session
#from flask import Flask, flash, redirect, render_template, request, session, abort, jsonify
import logging
from m2tool import process_urls_async, get_status, get_process_output
from flask_session import Session

app = Flask(__name__)
SESSION_TYPE = 'filesystem'
app.config.from_object(__name__)
Session(app)

procs = []

@app.route("/")
def index():
    return "Flask App!"

@app.route("/music/download")
def music_download():
  return render_template('music-download.html')

@app.route("/music/download/process", methods=['POST'])
def music_download_process():
  urlstr = request.form.to_dict(flat=True).get('urls')
  app.logger.info('music_download_process: ' + urlstr)
  p = process_urls_async(app, urlstr)
#    if not "p" in session:
  session["p"] = p
  return redirect("/music/download", code=302)

@app.route("/music/download/status", methods=['GET', 'POST'])
def music_download_status():
  return generate_msg(get_status(app, session["p"]))

@app.route("/music/download/status2", methods=['GET', 'POST'])
def music_download_status2():
  return generate_msg('status2')

def generate_msg(s):
  return render_template('message.html', message=s)


if __name__ == "__main__":
  app.run(host = '0.0.0.0', port=5000, debug=True)
