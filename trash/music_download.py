# https://pythonspot.com/flask-web-app-with-python/
# usage: python music-download.py
from flask import Flask, flash, redirect, render_template, request, session, abort, jsonify
import logging
from tool import process_urls_async, get_status, get_process_output
from threading import Thread, Lock

app = Flask(__name__)

lock = Lock()
procs = []

@app.route("/")
def index():
    return "Flask App!"

@app.route("/music/download")
def music_download():
  return render_template('music-download.html')

@app.route("/music/download/process", methods=['POST'])
def music_download_process():
  urlstr = request.form.to_dict(flat=True).get('urls')
  app.logger.info('music_download_process: ' + urlstr)
  thread = Thread(target=process_urls_async, args=(app, urlstr, lock, procs, ))
  thread.daemon = True
  thread.start()
  return redirect("/music/download", code=302)

@app.route("/music/download/status", methods=['GET', 'POST'])
def music_download_status():
  app.logger.info('procs: ' + str(procs[0]))
  if procs:
    get_status(app, procs[0])
  return generate_msg('done')

@app.route("/music/download/status2", methods=['GET', 'POST'])
def music_download_status2():
  if procs:
    get_process_output(app, procs[0])
  return generate_msg('done')

def generate_msg(s):
  return render_template('message.html', message=s)


if __name__ == "__main__":
  app.run(host = '0.0.0.0', port=5000, debug=True)
