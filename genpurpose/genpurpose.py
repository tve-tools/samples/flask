from flask import Flask
import logging

#from logging.config import dictConfig
#
#dictConfig(
#  {
#    "version": 1,
#    "formatters": {
#      "default": {
#        "format": "[%(asctime)s] [%(levelname)s | %(module)s] %(message)s",
#        "datefmt": "%B %d, %Y %H:%M:%S %Z",
#      },
#    },
#    "handlers": {
#      "console": {
#        "class": "logging.StreamHandler",
#        "formatter": "default",
#      },
#      "file": {
#        "class": "logging.FileHandler",
#        "filename": "/home/teggy/logs/genpurpose.log",
#        "formatter": "default",
#      },
#    },
#    "root": {"level": "DEBUG", "handlers": ["console", "file"]},
#  }
#)

app = Flask(__name__)
app.logger.setLevel(logging.DEBUG)

@app.route("/")
def hello():
  app.logger.info("Hello, World")
  app.logger.warning("Hello, World")
  return "<h1 style='color:blue'>Hello you!</h1>"

if __name__ == "__main__":
  app.run()
