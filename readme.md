pre-req
=======

```bash
# install the music_download flask app
sudo apt install python-pip
sudo pip install virtualenv
# create a virtual env and do the below pip install in this virtual env
pip install flask
pip install mutagen
pip install soco

```

# install the music_download service
```bash
sudo apt install daemonize

follow usage instructions from samples/configuration/linux/music_download.service

http://maisel02:5000/music/download/
```

# install fs cifs remote mount from maisel01
```bash
sudo vi /etc/fstab
//maisel01/music /net/maisel01/music cifs credentials=/root/.maisel01.cifs,iocharset=utf8,vers=1.0,uid=1000,noauto 0 0

sudo apt install -y cifs-utils

sudo mkdir -p /net/maisel01/music
sudo mount /net/maisel01/music
```

# install docker container for conversion
```bash
samplesng/dockerbox/youtube-dl $ docker build -t youtube-dl .

```


